<?php

session_start();

$_SESSION['ChatNick'] = "Mukta";

function showChatWindow(){
    echo "I'm inside the: ".__FUNCTION__;
    //show chat history box
    //chat text form
}

function showLoginBox(){
    echo "I'm inside the: ".__FUNCTION__;
    //get Nick Name from the Visitor
    //and save it to a session variable
}

if(isset($_SESSION["ChatNick"])){
    echo $_SESSION["ChatNick"];
    showChatWindow();
}
else{
   showLoginBox();
}

echo "<br>";


$situation = true;
if($situation == true){
    echo "The situation is true";
}
else{
    echo "The situation is false";
}

echo "<br>";


$x = 20;
$y = 67;

if($x>$y){
    echo "x is greater than y";
}
elseif($x<$y){
    echo "y is greater than x";
}
else{
    echo "x is equal to y";
}

echo "<br>";




$x = 35;
$y = 48;
$z = 25;

    if( ($x>$y) && ($x>$z) ){
        echo "x is the greatest";
    }

    elseif( ($y>$x) && ($y>$z) ) {
        echo "y is the greatest";
    }

    elseif( ($z>$x) && ($z>$y) ) {
        echo "z is the greatest";
    }

echo "<br>";

if( ($x<$y) && ($x<$z) ){
    echo "x is the smallest";
}

elseif( ($y<$x) && ($y<$z) ) {
    echo "y is the smallest";
}

elseif( ($z<$x) && ($z<$y) ) {
    echo "z is the smallest";
}

echo "<br>";


//switch case start
$result = 64;
switch($result){
    case ($result>=80) :
        echo "The result is A+";
        break;
    case ($result>=70 && $result<80) :
        echo "The result is A";
        break;
    case ($result>=60 && $result<70) :
        echo "The result is A-";
        break;
    case ($result>=50 && $result<60) :
        echo "The result is B";
        break;
    case ($result>=40 && $result<50) :
        echo "The result is C";
        break;
    case ($result>=33 && $result<40) :
        echo "The result is A";
        break;
    default:
        echo "Failed";
}

//switch case end
echo "<br>";

$situation = "valo"; //VALO or vAlO  //  Begotik, BEGtik
$situation = strtoupper($situation);
switch($situation){
    case "VALO";
        echo "The situation is good! Enjoy...<br>";
        break;
    case "BEGOTIK";
        echo "The situation is bad! Ran Away...<br>";
        break;
    default:
        echo "The situation is normal, Be careful<br>";
} //end of switch block